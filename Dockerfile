FROM ubuntu:latest
MAINTAINER Opad Yusuf "jjyusuf@gmail.com"
RUN apt-get update -y
RUN apt-get install -y python3 python3-dev python3-pip
COPY . /app
WORKDIR /app
RUN pip3 install -r requirements.txt
RUN chmod +x /app/run.sh
ENTRYPOINT ["/bin/bash", "/app/run.sh"]