from flask import Flask
from flask_restplus import Api, Resource, fields
from werkzeug.contrib.fixers import ProxyFix

app = Flask(__name__)
#app.wsgi_app = ProxyFix(app.wsgi_app)
api = Api(app, version='1.0', title='Rabbit API',
    description='A simple Registry of Rabbits API')

ns = api.namespace('rabbits', description='Rabbits operations')

rabbit = api.model('rabbits', {
    'id': fields.Integer(readonly=True, description='The rabbit unique identifier'),
    'name': fields.String(required=True, description='The rabbit name')
})


class Rabbit(object):
    def __init__(self):
        self.counter = 0
        self.rabbits = []

    def get(self, id):
        for rabbit in self.rabbits:
            if rabbit['id'] == id:
                return rabbit
        api.abort(404, "Todo {} doesn't exist".format(id))

    def create(self, data):
        rabbit = data
        rabbit['id'] = self.counter = self.counter + 1
        self.rabbits.append(rabbit)
        return rabbit

    def update(self, id, data):
        rabbit = self.get(id)
        rabbit.update(data)
        return rabbit

    def delete(self, id):
        rabbit = self.get(id)
        self.rabbits.remove(rabbit)


DAO = Rabbit()
DAO.create({'task': 'Build an API'})
DAO.create({'task': '?????'})
DAO.create({'task': 'profit!'})


@ns.route('/')
class RabbitList(Resource):
    '''Shows a list of all todos, and lets you POST to add new tasks'''
    @ns.doc('list_todos')
    @ns.marshal_list_with(rabbit)
    def get(self):
        '''List all tasks'''
        return DAO.rabbits

    @ns.doc('create_todo')
    @ns.expect(rabbit)
    @ns.marshal_with(rabbit, code=201)
    def post(self):
        '''Create a new task'''
        return DAO.create(api.payload), 201


@ns.route('/<int:id>')
@ns.response(404, 'Todo not found')
@ns.param('id', 'The task identifier')
class Todo(Resource):
    '''Show a single todo item and lets you delete them'''
    @ns.doc('get_todo')
    @ns.marshal_with(rabbit)
    def get(self, id):
        '''Fetch a given resource'''
        return DAO.get(id)

    @ns.doc('delete_todo')
    @ns.response(204, 'Todo deleted')
    def delete(self, id):
        '''Delete a task given its identifier'''
        DAO.delete(id)
        return '', 204

    @ns.expect(rabbit)
    @ns.marshal_with(rabbit)
    def put(self, id):
        '''Update a task given its identifier'''
        return DAO.update(id, api.payload)


if __name__ == '__main__':
    app.run(port=5000, host='0.0.0.0')
